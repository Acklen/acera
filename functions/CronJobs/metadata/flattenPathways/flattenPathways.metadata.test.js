const FlattenPathways = require("./flattenPathways.metadata");

describe("FlattenPathways metadata", () => {
  test("should get all flatten pathways", async () => {
    const flattenPathways = new FlattenPathways();

    const response = await flattenPathways.getAllFlattenPathways();

    expect(typeof response).toBe("object");
  });
});
