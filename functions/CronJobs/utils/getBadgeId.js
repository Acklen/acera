const getBadgeId = (str) => {
  if (str) {
    return str.substring(str.lastIndexOf("/") + 1);
  }
};

module.exports = getBadgeId;
