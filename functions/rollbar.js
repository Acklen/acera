const Rollbar = require('rollbar');
const envs = require('./env.json');

const ROLLBAR_USE = envs.service.rollbar_use;
const ROLLBAR_ACCESS_TOKEN = envs.service.rollbar_accessToken;
const ROLLBAR_CAPTURE_UNCAUGHT = envs.service.rollbar_captureUncaught;
const ROLLBAR_CAPTURE_UNHANDLED = envs.service.rollbar_captureUnhandledRejections;

var rollbar = new Rollbar({
  accessToken: ROLLBAR_ACCESS_TOKEN,
  captureUncaught: ROLLBAR_CAPTURE_UNCAUGHT,
  captureUnhandledRejections: ROLLBAR_CAPTURE_UNHANDLED
});

//Use a function in case wee need to add more logic for check this process
const isActivated = () => {
  if(ROLLBAR_USE){
    let message = "Rollbar activated succesfully"
    rollbar.log(message)
    console.log(message)
    return true
  }
  console.log("Rollbar it's not active")
  return false
}

// ERROR TYPES:
// - rollbar.critical()
// - rollbar.error()
// - rollbar.warning()
// - rollbar.info()
// - rollbar.debug()

module.exports = {
    rollbar: rollbar,
    isActivated:isActivated,
}