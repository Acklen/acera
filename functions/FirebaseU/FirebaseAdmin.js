const firebase = require('firebase-admin');
const env_json = require('../env.json');
const {getID} = require('./FirebaseUtils')

//var serviceAccount = require('./acera-credentials.json');

var serviceAccount = {
  "type": env_json.service.type,
  "project_id": env_json.service.project_id,
  "private_key_id": env_json.service.private_key_id.replace(/\\n/g, '\n'),
  "private_key": env_json.service.private_key_admin.replace(/\\n/g, '\n'),
  "client_email": env_json.service.client_email,
  "client_id": env_json.service.client_id_admin,
  "auth_uri": env_json.service.auth_uri,
  "token_uri": env_json.service.token_uri,
  "auth_provider_x509_cert_url": env_json.service.auth_provider_x509_cert_url,
  "client_x509_cert_url": env_json.service.client_x509_cert_url,
}


const snapshotToArray = (snapshot) => {
  const returnArr = [];
  snapshot.forEach(function (childSnapshot) {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};


const appauth = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: env_json.REACT_APP_FB_DATABASE ? envs.REACT_APP_FB_DATABASE : env_json.service.db_url,
});

const database = appauth.database()

//Badges

const addBadges = (data, time) => {
  //console.log("SAVING...", data.id)
  const badgeAux = {
    criteria:data.criteria? data.criteria:{id:"",narrative:"",url:""},
    description:data.description,
    issuer:data.issuer,
    id:data.id,
    name:data.name,
    tags:data.tags ? data.tags : [],
    savedate_7d: time
  }
  appauth.database().ref(`badges/${getID(badgeAux.id)}`).set(badgeAux);
};

const getBadges = (id) => {
  const promiseData = appauth.database().ref(`badges/${id}`);
  return promiseData;
};

const getGroupsAdmin = () => {
  const promiseData = appauth.database().ref("/groups");
  return promiseData;
};

const getUsers = () => {
    const promiseData = appauth.database().ref("/users");
    return promiseData;
};

const saveBackpackToken = (email, data, email2) => {
  var d = new Date();
  var n = d.getTime();
  appauth
    .database()
    .ref(
      `/users/${email.replace(/[\.@]/gi, "")}/backpacks/${email2.replace(
        /[\.@]/gi,
        ""
      )}`
    )
    .set({ data, issuedOn: n });
};

const getWebhooksByIssuer =async (issuer) => {
  let response = []
  await database
    .ref("/webhooks")
    .orderByChild("issuer")
    .equalTo(issuer)
    .once("value", (snapshot) => {
      response = snapshotToArray(snapshot);
    })
    .catch((e) => {
      error = e;
    });
  return response
}

module.exports = {
  addBadges: addBadges,
  getBadges: getBadges,
  getGroupsAdmin: getGroupsAdmin,
  getUsers: getUsers,
  saveBackpackToken: saveBackpackToken,
  database: database,
  getWebhooksByIssuer:getWebhooksByIssuer,
};
