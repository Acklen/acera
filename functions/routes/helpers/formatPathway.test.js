//const { modify } = require("./formatPathway");
//const fullstackPathway = require("./../../pathways/zmI_1cMZQvKnHDQpvgBVzQ.json");
//const Scenario4 = require("./../../pathways/U7bDGHhNSySaXNzetvhD9g.json");
//const Scenario5 = require("./../../pathways/jNwViywlRDepc-gXcrv5Kg.json");
//const databasePathway = require("./../database/pathway");
//const util = require("util");
describe("format Pathway", () => {
  /*test("should format the pathway", async () => {
    const [error, pathways] = await databasePathway.getAllPathwaysInDatabase();
    const expected = {
      zmI_1cMZQvKnHDQpvgBVzQ: {
        completionBadge:
          "https://acklenavenue.badgr.com/public/badges/zmI_1cMZQvKnHDQpvgBVzQ",
        title: "Full-Stack Developer",
        children: [
          {
            children: [
              {
                children: [
                  {
                    requiredBadge:
                      "https://badgr.com/public/badges/MKTnwpu_TaaTTd6V7o4LtQ",
                    title: "Self-Evaluation",
                    children: [
                      {
                        requiredBadge:
                          "https://acklenavenue.badgr.com/public/badges/j4hIuU1mSvyk8Q1Xi0dhmw",
                        title: "Professional Skills",
                      },
                    ],
                  },
                  {
                    requiredBadge:
                      "https://badgr.com/public/badges/1dPHojYCQNydNqlWwQ7hZg",
                    title: "English Communicator V2",
                    children: [
                      {
                        requiredBadge:
                          "https://acklenavenue.badgr.com/public/badges/j4hIuU1mSvyk8Q1Xi0dhmw",
                        title: "Professional Skills",
                      },
                    ],
                  },
                  {
                    requiredBadge:
                      "https://badgr.com/public/badges/dMIQ5JEzT4e2diO4jJ_5hA",
                    title: "Emotionally Intelligent",
                    children: [
                      {
                        requiredBadge:
                          "https://acklenavenue.badgr.com/public/badges/j4hIuU1mSvyk8Q1Xi0dhmw",
                        title: "Professional Skills",
                      },
                    ],
                  },
                ],
                requiredBadge:
                  "https://badgr.com/public/badges/k5G85n4SR0ClIVmVtPxD5A",
                title: "360 Feedback Receiver",
              },
            ],
            pathwayURL: "pathway/bXQp-ii-SP-mdOoY9hRWvQ",
            requiredBadge:
              "https://acklenavenue.badgr.com/public/badges/bXQp-ii-SP-mdOoY9hRWvQ",
            title: "Personal Growth",
          },
          {
            children: [
              {
                children: [
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/-AXf3JrPSsKZ50-qiMj2xA",
                    title: "Adapter Pattern",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/X9R3b7psSiGDA4OowV9Wpw",
                    title: "Strategy Pattern",
                  },
                ],
                title: "Design Patterns",
              },
              {
                children: [
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/upn2ozyZRNmui7F4SJmsuQ",
                    title: "Git",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/hpvFRz6yTtu2VvQPyA8LFg",
                    title: "Code Editor",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/hpvFRz6yTtu2VvQPyA8LFg",
                    title: "Terminal",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/ebAZzcYNQ7iTq2Bz8C7pQA",
                    title: "Trello",
                  },
                ],
                title: "Tools",
              },
              {
                children: [
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/mpP6EehkQKWaLMNdlHof7A",
                    title: "SPA Front-End",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/2d_P5eloSwSxjxyhYEoF-A",
                    title: "REST API Back-End",
                  },
                ],
                completionBadge:
                  "https://acklenavenue.badgr.com/public/assertions/fygDwVQkQe--skqXNqsPeQ",
                title: "Applications",
              },
              {
                children: [
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/okTiXkVERH-cNJ9dtg2V4Q",
                    title: "SQL",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/Qup2615jQoi0WAdyO97zPw",
                    title: "C#",
                  },
                  {
                    requiredBadge:
                      "https://acklenavenue.badgr.com/public/badges/vKmoeMs2Q-2nFVhYpCufUA",
                    title: "Javascript",
                  },
                ],
                title: "Languages",
              },
            ],
            title: "Technical Skills",
          },
        ],
      },
    };
    const response = modify(Object.values(fullstackPathway)[0], pathways);
    expect(response).toStrictEqual(expected);
  });
  test("should format the pathway", async () => {
    const [error, pathways] = await databasePathway.getAllPathwaysInDatabase();
    const expected = {
      U7bDGHhNSySaXNzetvhD9g: {
        children: [
          {
            children: [
              {
                requiredBadge:
                  "https://badgr.com/public/badges/I4w9NAXTTLqD4YQO2WCIEw",
                title: "Acera Test Badge 18",
              },
            ],
            title: "Parent Node 1",
          },
          {
            pathwayURL: "pathway/2icBqJ_RRDC4aaAko8PzVA",
            completionBadge:
              "https://badgr.com/public/badges/SWGGaaiwTDSZpV0_YLp5PA",
            title: "Child pathway",
            children: [
              {
                requiredBadge:
                  "https://badgr.com/public/badges/kUDZqJS0TE-WRRweXHf2HQ",
                title: "Acera Test Badge 15",
              },
            ],
          },
        ],
        title: "Acera-Test-Card94-Rework2-Scenario5",
        completionBadge:
          "https://badgr.com/public/badges/U7bDGHhNSySaXNzetvhD9g",
      },
    };

    const response = modify(Object.values(Scenario4)[0], pathways);
    expect(response).toStrictEqual(expected);
  });
  test("should format the pathway", async () => {
    const [error, pathways] = await databasePathway.getAllPathwaysInDatabase();
    const expected = {
      "jNwViywlRDepc-gXcrv5Kg": {
        children: [
          {
            children: [
              {
                requiredBadge:
                  "https://badgr.com/public/badges/SKHgytuvSsiMdBBccRNEhQ",
                title: "Acera Test Badge 58",
              },
            ],
            title: "Parent Node 1",
          },
          {
            pathwayURL: "pathway/54UbPa8aQi2DvHdIOylc_w",
            title: "Acera Test Card94 Rework2 override",
            completionBadge:
              "https://badgr.com/public/badges/54UbPa8aQi2DvHdIOylc_w",
            children: [
              {
                requiredBadge:
                  "https://badgr.com/public/badges/cT2xKjx4QVeTQOZGrS6jLg",
                title: "Acera Test Badge 2",
              },
            ],
          },
        ],
        title: "Acera-Test-Card94-Rework2-Scenario4",
        completionBadge:
          "https://badgr.com/public/badges/jNwViywlRDepc-gXcrv5Kg",
      },
    };

    const response = modify(Object.values(Scenario5)[0], pathways);
    expect(response).toStrictEqual(expected);
  });*/
});
