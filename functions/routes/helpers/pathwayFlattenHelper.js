const getID = (str) => {
  if (str) {
    return str.substring(str.lastIndexOf("/") + 1);
  }
};

const processChild = (child) => {
  if (child.children) {
    let needs = [];
    child.children.map((subChild) => {
      if (subChild.children) {
        let subNeeds = [];
        let id;
        let type;
        if (subChild.completionBadge) {
          type = "completionBadge";
          id = getID(subChild.completionBadge);
        } else if (subChild.requiredBadge) {
          type = "requiredBadge";
          id = getID(subChild.requiredBadge);
        } else {
          type = "NotRequired";
          id = subChild.title;
        }

        subChild.children.map((subc) => {
          let id;
          if (subChild.requiredBadge) {
            id = getID(subChild.requiredBadge);
          } else if (subChild.completionBadge) {
            id = getID(subChild.completionBadge);
          } else {
            id = subChild.title;
          }
          const obj = processChild(subc);
          subNeeds.push(obj);
        });
        if(subChild.requiredNumber){
          needs.push({ id: id, type: type, requiredNumber: subChild.requiredNumber, needs: subNeeds });
        }else{
          needs.push({ id: id, type: type, needs: subNeeds });
        }
      } else {
        let subChildID;
        if (subChild.requiredBadge) {
          subChildID = getID(subChild.requiredBadge);
        } else if (subChild.completionBadge) {
          id = getID(subChild.completionBadge);
        } else {
          subChildID = subChild.title;
        }
        needs.push(subChildID);
      }
    });
    let id;
    let type;
    if (child.completionBadge) {
      if(child.requiredBadge){
        needs.push(getID(child.requiredBadge));
      }
      type = "completionBadge";
      id = getID(child.completionBadge);
    } else if (child.requiredBadge) {
      type = "requiredBadge";
      id = getID(child.requiredBadge);
    } else {
      type = "NotRequired";
      id = child.title;
    }
    if(child.requiredNumber){
      return {
        id: id,
        type: type,
        requiredNumber: child.requiredNumber,
        needs: needs,
      };
    }else{
      return {
        id: id,
        type: type,
        needs: needs,
      };
    }
  } else {
    return {
      id: getID(child.requiredBadge),
      needs: [""],
    };
  }
};

const reformatOutputChildren = (children, values) => {
  for (let child of children) {
    if (child.needs !== undefined) {
      let arr = child.needs.map(function (need) {
        if (need.id) {
          return need.id;
        } else {
          return need;
        }
      });
      let obj;
      if (child.type) {
        if(child.requiredNumber){
          obj = {
            id: child.id,
            type: child.type,
            requiredNumber: child.requiredNumber,
            needs: arr,
          };
        }else{
          obj = {
            id: child.id,
            type: child.type,
            needs: arr,
          };
        }
      } else {
        obj = {
          id: child.id,
          needs: arr,
        };
      }
      if (obj.needs[0] !== "") {
        values.push(obj);
      }
    }
  }
  for (let child of children) {
    if (child.needs) {
      reformatOutputChildren(child.needs, values);
    }
  }
};

const reformatOutput = (output) => {
  let result = [];
  let head = {};
  if(output[0]["requiredNumber"]){
    head = {
      id: output[0]["id"],
      type: "completionBadge",
      requiredNumber: output[0]["requiredNumber"],
      needs: output[0]["needs"].map((a) => a.id),
    };
    if(output[0]["requiredBadge"]){
      head.needs.push(output[0]["requiredBadge"]);
    }
  }else{
    head = {
      id: output[0]["id"],
      type: "completionBadge",
      needs: output[0]["needs"].map((a) => a.id),
    };
    if(output[0]["requiredBadge"]){
      head.needs.push(output[0]["requiredBadge"]);
    }
  }
  result.push(head);
  let needs = output[0]["needs"];
  let values = [];
  reformatOutputChildren(needs, values);
  result = result.concat(values);
  return result;
};

const pathwayMapTransform = (pathway, result) => {
  let keys = Object.keys(pathway);
  let pathwayID = keys[0]; // completionBadge
  let pathwayContent = Object.values(pathway)[0];
  if (pathwayContent.children) {
    let res = [];
    if(pathwayContent.requiredBadge){
      let required = {
        id: getID(pathwayContent.requiredBadge),
        type: "requiredBadge",
        needs: []
      }
      res.push(required);
    }
    pathwayContent.children.map((child) => {
      res.push(processChild(child));
    });
    if(pathwayContent.requiredNumber){
      result.push({ id: pathwayID, requiredNumber: pathwayContent.requiredNumber, needs: res });
    }else{
      result.push({ id: pathwayID, needs: res });
    }
  } else {
    const badgeReq = { id: pathwayID, needs: [] };
    result.push(badgeReq);
  }
  return reformatOutput(result);
};

module.exports = pathwayMapTransform;
