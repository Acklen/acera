function Ok(message) {
    return {
        StatusCode: 201,
        StatusMessage: {
            ok:`${message}`
        }
    }
}

module.exports = Ok