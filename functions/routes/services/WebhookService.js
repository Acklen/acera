const fetch = require("node-fetch");
const logger = require("firebase-functions").logger;
const FirebaseAdmin = require("../../FirebaseU/FirebaseAdmin");

const runAwardWebhooks = async (response) => {
  let badgeInfo = {};
  if (response.result.length > 0 && response.result[0].badgeclassOpenBadgeId) {
    await fetch(response.result[0].badgeclassOpenBadgeId)
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        (badgeInfo = res), logger.info("res", res, { structuredData: true });
      });
  }
  const webhooksToTrigger = await FirebaseAdmin.getWebhooksByIssuer(
    response.result[0].issuer
  );
  logger.info("webhooksToTrigger",webhooksToTrigger,{structuredData:true})
  const webhookData = {
    badgeInfo,
    awardInfo: response.result[0],
  };
  webhooksToTrigger.forEach((webhook) => {
    fetch(webhook.url, {
      method: "POST",
      body: JSON.stringify(webhookData),
      headers: {
        "Content-Type": "application/json",
      },
    });
  });
};

const WebhookService = {
  runAwardWebhooks: runAwardWebhooks,
};

module.exports = WebhookService;
