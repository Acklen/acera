const axios = require('axios');
const envs = require('../../env.json');
const {addBadges, getBadges} = require("../../FirebaseU/FirebaseAdmin");
const rollbar = require('../../rollbar.js')

const ISSUER_ID = envs.service.issuer_id
const ONE_WEEK = 60 * 60 * 1000 * 24 * 7;

const BadgeService = {
    getBadgeData: async(badgeToken, authToken) => {
        let response;
        var d = new Date(); 
        var time = d.getTime();

        let badgeSnap = await getBadges(badgeToken).once('value')
        if (badgeSnap.val() && (time - badgeSnap.val().savedate_7d) < ONE_WEEK) {
            return response = badgeSnap.val()
        }else{
            await axios.get(`https://api.badgr.io/public/badges/${badgeToken}`).then(res => {
                addBadges(res.data, time)          
                response = res.data
            }).catch(err => {
                console.log(err);
                if(rollbar.isActivated){
                    rollbar.rollbar.error("BadgeService Fails " + err)
                }
            })
            return response
        }
    },
    getBadges: async(authToken) => {
        let response;
        
        await axios({
            headers: {
                'Authorization': `Bearer ${authToken}`
            },
            method: 'get',
            url: `/issuers/${ISSUER_ID}/badgeclasses`,

        }).then(res => {                   
            response = res.data
        }).catch(err => {  
            console.log(err)
            if(rollbar.isActivated){
                rollbar.rollbar.error("getBadges Fails " + err)
            }
        })

        return response
    },
    getAllBadges: async(issuerToken, authToken)=>{
        let response;

        await axios({
            headers:{
                'Authorization': `Bearer ${authToken}`
            },
            method:'get',
            url: '/badgeclasses'
        }).then(res => {     
            console.log("SEE RES: ",res);        
        }).catch(err => {
            console.log(err)
            if(rollbar.isActivated){
                rollbar.rollbar.error("getAllBadges Fails " + err)
            }
        })

        return response
    }
}

module.exports = BadgeService;