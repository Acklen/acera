const MissingParamError = require("../routes/helpers/MissingParamError");
const BadRequest = require("../routes/helpers/BadRequest");
const validator = require("validator");

const isURL = (url) => validator.isURL(url);

const validateChildrens = (children) => {
  const childrenFields = [
  ];
  for (const child of children) {
    for (const childrenField of childrenFields) {
      if (typeof child[childrenField] !== "undefined") {
        if (!child[childrenField]) {
          const missingParamError = MissingParamError(childrenField);
          return [false, missingParamError, null];
        }
      }
    }
    if (typeof child["completionBadge"] !== "undefined") {
      if (child["completionBadge"].length>0 && !isURL(child["completionBadge"])) {
        const badRequest = BadRequest("completionBadge must be a valid url.");
        return [false, badRequest, null];
      }
    }
    if (typeof child["requiredBadge"] !== "undefined") {
      if (child["requiredBadge"].length>0 && !isURL(child["requiredBadge"])) {
        const badRequest = BadRequest("requiredBadge must be a valid url.");
        return [false, badRequest, null];
      }
    }
    if (typeof child["children"] !== "undefined") {
      children = child["children"];
      const [error, errorMsg] = validateChildrens(children);
      if (error === false) {
        return [error, errorMsg, null];
      }
    }
    if (typeof child["children"] === "undefined") {
      children = null;
    }
  }
  return [true, "ok", children];
};

class PathwayModel {
  id;
  requiredNumber;
  completionBadge;
  pathwayURL;
  title;
  requiredBadge;
  constructor(
    id,
    requiredNumber,
    completionBadge,
    pathwayURL,
    title,
    requiredBadge
  ) {
    this.id = id;
    this.requiredNumber = requiredNumber;
    this.completionBadge = completionBadge;
    this.pathwayURL = pathwayURL;
    this.title = title;
    this.requiredBadge = requiredBadge;
  }
  validate(pathway) {
    const pathwayValues = Object.values(pathway)[0];
    pathwayValues.requiredNumber = pathwayValues.requiredNumber
      ? pathwayValues.requiredNumber
      : "0";

    const fields = ["title","completionBadge"];
    for (const field of fields) {
      if (!pathwayValues[field]) {
        const missingParamError = MissingParamError(field);
        return [false, missingParamError];
      }
    }
    if (pathwayValues["completionBadge"].length>0 && isURL(pathwayValues["completionBadge"]) !== true) {
      const badRequest = BadRequest("completionBadge must be a valid url.");
      return [false, badRequest];
    }

    // children validation
    let children = pathwayValues["children"];
    while (children) {
      const childExists = children.some(
        (child) => Object.keys(child)[0] === "children"
      );
      if (childExists) {
        const [error, errorMsg, childrenMsg] = validateChildrens(children);
        if (!error) {
          return [error, errorMsg];
        }
        if (error) {
          children = childrenMsg;
        }
      } else {
        const [error, errorMsg] = validateChildrens(children);
        if (!error) {
          return [error, errorMsg];
        }
        children = null;
      }
    }
    return [true, pathway];
  }
}

module.exports = PathwayModel;
