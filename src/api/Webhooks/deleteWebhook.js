import { db } from "../firebase";

export const deleteWebhook = async (id) => {
  await db.ref(`/webhooks/${id}`).remove();
};
